package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class threadControllerTests {
    private Thread thread;

    @BeforeEach
    @DisplayName("thread creation test")
    void createThreadTest() {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        thread = new Thread("somebody", timestamp, "forum", "message", "slug", "title", 5);
        thread.setId(0);
    }

    @Test
    @DisplayName("Checking id,slug")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            assertEquals(thread, controller.CheckIdOrSlug("slug"));
            assertEquals(thread, controller.CheckIdOrSlug("0"));
        }
    }

    @Test
    @DisplayName("Create post")
    void testCreatePost() {
        List<Post> posts = new ArrayList<>();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts), controller.createPost("slug", posts));
        }
    }

    @Test
    @DisplayName("Get posts")
    void testGetPosts() {
        List<Post> posts = new ArrayList<>();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getPosts(thread.getId(), 100, 1, null, false)).thenReturn(posts);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts), controller.Posts("slug", 10, 1, "sort", true));
        }
    }

    @Test
    @DisplayName("Change")
    void testChange() {
        Thread changedThread = new Thread("user", new Timestamp(100), "forum", "message", "slug2", "title", 101);
        changedThread.setId(1);

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug2")).thenReturn(changedThread);
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.change("slug2", thread), "thread was changed");

        }
    }

    @Test
    @DisplayName("Info")
    void testInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.info("slug"));
        }
    }

    @Test
    @DisplayName("Voting")
    void testCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                User user = new User("user", "someEmail", "someName", "someDescription");
                Vote vote = new Vote("user", 100);
                threadController controller = new threadController();

                threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                userMock.when(() -> UserDAO.Info("user")).thenReturn(user);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread).getStatusCode(), controller.createVote("slug", vote).getStatusCode());
            }
        }
    }
}